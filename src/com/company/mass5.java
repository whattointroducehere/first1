package com.company;

import java.util.Random;

public class mass5 {                    // Посчитать сумму элементов массива с нечетными индексами/четным индексом
    public static void mass5(String[] args) {
        int[] mass = RandomMass(10, 10);

        int sumchet = 0;
        int sumnechet = 0;
        for (int i = 0; i < mass.length; i++) {
            if (i % 2 == 0) {
                sumchet += mass[i];
            } else {
                sumnechet += mass[i];
            }
        }

        System.out.println("Сумма четных "+sumchet);
        System.out.println("Сумма нечетных "+sumnechet);

    }

    private static int[] RandomMass(int size, int bound) {
        int[] mass = new int[size];
        Random rnd = new Random();
        for (int i = 0; i < mass.length; i++) {
            mass[i] = rnd.nextInt(bound);
        }
        return mass;
    }
}
